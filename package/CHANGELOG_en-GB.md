## 6.5.8

* Scroll error fixed
* CSS Root variables moved (better scoping)
* Constant handling improved
