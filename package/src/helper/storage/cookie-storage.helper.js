export default class CookieStorage {

    /**
     * returns if cookies are supported
     *
     * @returns {boolean}
     */
    static isSupported() {
        return document.cookie !== 'undefined';
    }

    /**
     * Sets cookie with name, value and expiration date
     *
     * @param {string} key
     * @param {string} value
     *
     * @param {number} expirationDays
     */
    static setItem(key, value, expirationDays) {
        if (typeof key === 'undefined' || key === null) {
            throw new Error('You must specify a key to set a cookie');
        }

        const date = new Date();
        date.setTime(date.getTime() + (expirationDays * 24 * 60 * 60 * 1000));

        let secure = '';
        if (location.protocol === 'https:') {
            secure = 'secure';
        }

        document.cookie = `${key}=${value};expires=${date.toUTCString()};path=/;sameSite=lax;${secure}`;
    }

    /**
     * Gets cookie value through the cookie name
     *
     * @param {string} key
     *
     * @returns {string|false} cookieValue
     */
    static getItem(key) {
        if (!key) {
            return false;
        }

        const name = key + '=';
        const allCookies = document.cookie.split(';');

        for (let i = 0; i < allCookies.length; i++) {
            let singleCookie = allCookies[i];

            while (singleCookie.charAt(0) === ' ') {
                singleCookie = singleCookie.substring(1);
            }

            if (singleCookie.indexOf(name) === 0) {
                return singleCookie.substring(name.length, singleCookie.length);
            }
        }

        return false;
    }

    /**
     * Get a map of all cookies
     * @returns {Map<string, string>}
     */
    static getAll() {
        const cookies = new Map();
        const allCookies = document.cookie.split(';');

        allCookies.forEach((singleCookie) => {
            singleCookie = singleCookie.charAt(0) === ' ' ? singleCookie.substring(1) : singleCookie;

            const [key, value] = singleCookie.split('=');

            cookies.set(key, value);
        })

        return cookies
    }

    /**
     * removes a cookie
     *
     * Startswith Regex: /^value-/
     * Endswith Regex: /-value$/
     * Contains Regex: /value/
     *
     * @param key
     */
    static removeItem(key) {
        const cookies = CookieStorage.getAll();

        for (let [cookie] of cookies) {
            if ((key instanceof RegExp && cookie.match(key)) || cookie === key) {
                document.cookie = `${cookie}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT;path=/`;
            }
        }
    }

    /**
     * removes one or multiple cookies by string or regex
     *
     * Startswith Regex: /^value-/
     * Endswith Regex: /-value$/
     * Contains Regex: /value/
     *
     * @param keys
     */
    static removeItems(...keys) {
        keys.forEach((key) => CookieStorage.removeItem(key))
    }

    /**
     * Remove all cookies
     *
     * @returns {string}
     */
    static clear() {
        const cookies = CookieStorage.getAll();

        for (let [cookie] of cookies) {
            document.cookie = `${cookie}= ; expires = Thu, 01 Jan 1970 00:00:00 GMT;path=/`;
        }
    }
}
