import {TinySliderPlugin, ImageCompareSliderPlugin} from './slider'
import {BasicCaptchaPlugin, GoogleReCaptchaV2Plugin, GoogleReCaptchaV3Plugin} from './sw'
import ScrollUpPlugin from "./scroll-up/scroll-up.plugin";
import CountUpPlugin from "./count-up/count-up.plugin";
import ProgressbarPlugin from "./progressbar/progressbar";
import InfoCirclePlugin from "./info-circle/info-circle.plugin";
import {TimelinePlugin, TimelineOutputPlugin} from "./timeline";
import {TabmodulePlugin, TabmoduleOutputPlugin} from "./tabmodule";


export {
    TinySliderPlugin,
    ImageCompareSliderPlugin,
    BasicCaptchaPlugin,
    GoogleReCaptchaV2Plugin,
    GoogleReCaptchaV3Plugin,
    ScrollUpPlugin,
    CountUpPlugin,
    ProgressbarPlugin,
    InfoCirclePlugin,
    TimelinePlugin,
    TimelineOutputPlugin,
    TabmodulePlugin,
    TabmoduleOutputPlugin
}
