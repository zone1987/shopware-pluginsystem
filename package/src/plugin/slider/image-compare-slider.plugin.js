import {Plugin} from '../../plugin-system';
import ImageCompare from "image-compare-viewer";
import 'image-compare-viewer/dist/image-compare-viewer.min.css'
export default class ImageCompareSliderPlugin extends Plugin {

    static options = {
        controlColor: "#FFFFFF",
        controlShadow: true,
        addCircle: false,
        addCircleBlur: true,
        showLabels: false,
        labelOptions: {
            before: 'Before',
            after: 'After',
            onHover: false
        },
        smoothing: false,
        smoothingAmount: 100,
        hoverStart: false,
        verticalMode: false,
        startingPoint: 50,
        fluidMode: true
    };

    init() {
        new ImageCompare(this.el, this.options).mount();
    }
}