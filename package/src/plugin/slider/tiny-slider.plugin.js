import {BaseSliderPlugin} from "../sw";
import deepmerge from "deepmerge";
import { tns } from 'tiny-slider';

export default class TinySliderPlugin extends BaseSliderPlugin {

    static options = deepmerge(BaseSliderPlugin.options, {
        initializedCls: 'tiny-slider-initialized',
        containerSelector: '[data-tiny-slider-container=true]',
        controlsSelector: '[data-tiny-slider-controls=true]',
        navSelector: '[data-tiny-slider-nav=true]'
    })

    init() {
        super.init();
    }

    /**
     * initialize the slider
     *
     * @private
     */
    _initSlider() {
        this.el.classList.add(this.options.initializedCls);

        const container = this.el.querySelector(this.options.containerSelector);
        const controlsContainer = this.el.querySelector(this.options.controlsSelector);
        const navContainer = this.el.querySelector(this.options.navSelector);

        if ((navContainer && this._sliderSettings.nav !== undefined) || this._alternateNav === true) {
            this._sliderSettings.nav = false;
            this._setNavigationDots(navContainer);
            this._alternateNav = true;
        }

        const onInit = () => {
            PluginManager.initializePlugins();

            this.$emitter.publish('initSlider');
        };

        if (container) {
            if (this._sliderSettings.enabled) {
                container.style.display = '';
                this._slider = tns({
                    container,
                    controlsContainer,
                    onInit,
                    ...this._sliderSettings,
                });

                if (this._buttons !== undefined) {
                    this._setActiveNavigationDot();

                    if (controlsContainer) {
                        const controlButtons = controlsContainer.querySelectorAll('button');

                        controlButtons.forEach((controlButton) => {
                            controlButton.addEventListener('click', this._setActiveNavigationDot.bind(this, true))
                        })
                    }
                }
            } else {
                container.style.display = 'none';
            }
        }

        this.$emitter.publish('afterInitSlider');
    }

    _setActiveNavigationDot(isControl = false) {
        this._activeNav = Array.from(this._buttons).find((node) => {
            if (node.dataset.nav !== undefined) {
                const currentIndex = isControl
                    ? (this._getCurrentIndex() + 1)
                    : (this._getCurrentIndex() <= 1 ? 1 : this._getCurrentIndex() - 1)
                return parseInt(node.dataset.nav) === currentIndex;
            }

            return false;
        });

        this._buttons.forEach((button) => button.classList.remove('tns-nav-active'));

        if (this._activeNav !== undefined) {
            this._activeNav.classList.add('tns-nav-active');
        }
    }

    _setNavigationDots(navContainer) {
        this._buttons = navContainer.querySelectorAll('button');

        this._buttons.forEach((button) => {
            button.addEventListener('click', this._onClickDot.bind(this))
        })
    }

    _onClickDot(e) {
        const target = e.target;
        const slideIndex = target.dataset.nav;

        this._buttons.forEach((button) => button.classList.remove('tns-nav-active'));

        if (slideIndex !== undefined) {
            this._slider.goTo(parseInt(slideIndex) - 1)
            target.classList.add('tns-nav-active');
        }
    }
}
