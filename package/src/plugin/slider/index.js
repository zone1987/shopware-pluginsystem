import TinySliderPlugin from "./tiny-slider.plugin";
import ImageCompareSliderPlugin from "./image-compare-slider.plugin";

export {
    TinySliderPlugin,
    ImageCompareSliderPlugin
}
