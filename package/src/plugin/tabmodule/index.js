import TabmodulePlugin from "./tabmodule.plugin";
import TabmoduleOutputPlugin from "./tabmodule-output.plugin";

export {
    TabmodulePlugin,
    TabmoduleOutputPlugin
}
