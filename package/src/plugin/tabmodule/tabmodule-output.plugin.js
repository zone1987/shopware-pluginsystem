import {Plugin} from '../../plugin-system';
import {DomAccess, StringHelper} from "../../helper";
import './tabmodule.output.scss';

export default class TabmoduleOutputPlugin extends Plugin {

    static options = Object.preventExtensions({

    })

    static _currentItemIndex = 0;

    init() {
        this._currentItemIndex = 0;

        if (this.el.dataset.tabmoduleId !== undefined) {
            document.$emitter.subscribe('tabmodule-registered', (event) => {

                this._toggleElements();

                if (event.detail.id === this.el.dataset.tabmoduleId) {
                    document.$emitter.subscribe('tabmodule-changed', (event) => {
                        this._currentItemIndex = event.detail.itemIndex;
                        this._toggleElements();
                    })
                }
            })
        } else {
            throw new Error('You need to provide an id for the tabmodule output')
        }
    }

    _prepareElements() {
        const allItems = DomAccess.querySelectorAll(this.el, '[data-tabmodule-output-id]', false);

        if (allItems) {
            Array.from(allItems).forEach((item) => {
                if (item.dataset.tabmoduleOutputId.length > 0) {
                    let ids = StringHelper.isJson(item.dataset.tabmoduleOutputId)
                        ? JSON.parse(item.dataset.tabmoduleOutputId)
                        : item.dataset.tabmoduleOutputId;

                    if (Array.isArray(ids)) {
                        ids.forEach((id) => {
                            const tmp = item.cloneNode(true);
                            tmp.setAttribute('data-tabmodule-output-id', id);

                            item.parentNode.insertBefore(tmp, item);
                        })

                        item.parentNode.removeChild(item)
                    }
                }
            })
        }
    }

    _toggleElements() {
        this._prepareElements();

        const currentSelector = `[data-tabmodule-output-id="${this._currentItemIndex}"]`;

        const allItems = DomAccess.querySelectorAll(this.el, '[data-tabmodule-output-id]', false);
        const target = DomAccess.querySelector(this.el, currentSelector, false);


        if (allItems) {
            Array.from(allItems).forEach((item) => {
                if (item.classList.contains('active')) {
                    item.classList.remove('active');
                }
            })
        }

        if (target) {
            if (!target.classList.contains('active')) {
                target.classList.add('active');
            }
        }
    }
}
