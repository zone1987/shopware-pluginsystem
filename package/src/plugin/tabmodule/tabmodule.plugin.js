import {Plugin} from '../../plugin-system';
import {DomAccess} from "../../helper";
import './tabmodule.scss'

export default class TabmodulePlugin extends Plugin {

    static options = Object.preventExtensions({
        itemSelector: '.tabmodule-item'
    })

    static _id = null;
    static _items = null;

    init() {
        this._id = null;
        this._items = [];

        if (this.el.dataset.tabmoduleId !== undefined) {
            this._id = this.el.dataset.tabmoduleId;

            this._registerTabmodule()
            this._loadItems();
        } else {
            throw new Error('You need to provide an id for the tabmodule')
        }
    }

    _registerTabmodule() {
        document.$emitter.publish('tabmodule-registered', {
            id: this._id
        });
    }

    _loadItems() {
        this._items = DomAccess.querySelectorAll(this.el, this.options.itemSelector, false);

        if (this._items) {
            Array.from(this._items).forEach((item, key) => {
                if (item.dataset.tabmoduleItemId === undefined) {
                    item.setAttribute('data-tabmodule-item-id', key);
                }

                if (key === 0) {
                    item.classList.add('active');
                    document.$emitter.publish('tabmodule-changed', {
                        itemIndex: item.dataset.tabmoduleItemId
                    });
                }

                item.addEventListener('click', this._onClickItem.bind(this));
            });
        }
    }

    _onClickItem(e) {
        Array.from(this._items).forEach((i) => i.classList.remove('active'));

        e.target.classList.add('active');
        document.$emitter.publish('tabmodule-changed', {
            itemIndex: e.target.dataset.tabmoduleItemId
        });
    }
}
