import {Plugin} from '../../plugin-system';
import {DomAccess} from "../../helper";
import './timeline.scss'
import {SCROLL_BEHAVIOR} from "../../constant";

export default class TimelinePlugin extends Plugin {

    static options = Object.preventExtensions({
        // Selectors
        itemSelector: '.timeline-item',
        itemsWrapperSelector: '.timeline-items',
        itemHeadlineSelector: '.timeline-item-headline',
        itemContentSelector: '.timeline-item-content',
        itemDividerSelector: 'timeline-item-divider',
        controlPrevSelector: '.timeline-controls-prev',
        controlNextSelector: '.timeline-controls-next',

        // Options
        passCssVariables: true,
        scrollBehavior: SCROLL_BEHAVIOR.SMOOTH,

        // Css Options
        timelineJuctionSize: '16px',
        timelineJuctionColor: '#007BC4',
        timelineLineStrength: '1px',
        timelineLineColor: '#B3B3B3',
        timelineLineSpace: '5px',
        timelineLinePadding: '10px',
        timelineDividerLength: '60%',
        timelineDividerColor: '#007BC4',
        timelineControlsPadding: '50px'
    })

    static _id = null;
    static _currentItemIndex = 0;
    static _totalHeight = 0;
    static _items = [];
    static _lastScrollTop = 0;
    static _positions = []

    init() {
        this._id = null;
        this._currentItemIndex = 0;
        this._totalHeight = 0;
        this._items = [];
        this._lastScrollTop = 0;
        this._positions = [];

        if (this.options.passCssVariables) {
            this.el.style.setProperty('--timeline-junction-size', this.options.timelineJuctionSize);
            this.el.style.setProperty('--timeline-junction-color', this.options.timelineJuctionColor);
            this.el.style.setProperty('--timeline-line-strength', this.options.timelineLineStrength);
            this.el.style.setProperty('--timeline-line-color', this.options.timelineLineColor);
            this.el.style.setProperty('--timeline-line-space', this.options.timelineLineSpace);
            this.el.style.setProperty('--timeline-left-padding', this.options.timelineLinePadding);
            this.el.style.setProperty('--timeline-divider-length', this.options.timelineDividerLength);
            this.el.style.setProperty('--timeline-divider-color', this.options.timelineDividerColor);
            this.el.style.setProperty('--timeline-controls-padding', this.options.timelineControlsPadding);
        }

        if (this.el.dataset.timelineId !== undefined) {
            this._id = this.el.dataset.timelineId;

            this._registerTimeline()

            this._prevButton = DomAccess.querySelector(this.el, this.options.controlPrevSelector, false);
            this._nextButton = DomAccess.querySelector(this.el, this.options.controlNextSelector, false);

            this._itemWrapper = DomAccess.querySelector(this.el, this.options.itemsWrapperSelector, false);

            if (this._itemWrapper) {
                this._itemWrapper.scrollTop = 0;
            }

            this._loadItems();

            if (this._prevButton) {
                this._prevButton.setAttribute('disabled', '');
            }

            this._registerEvents();

            document.onload = () => {
                document.$emitter.publish('timeline-index-changed', {
                    itemIndex: 0
                });
            }
        } else {
            throw new Error('You need to provide an id for the timeline')
        }
    }

    _registerTimeline() {
        document.$emitter.publish('timeline-registered', {
            id: this._id
        });
    }

    _registerEvents() {
        if (this._prevButton && this._nextButton) {
            this._prevButton.addEventListener('click', this.onPrevButtonClicked.bind(this))
            this._nextButton.addEventListener('click', this.onNextButtonClicked.bind(this))
        }
    }

    onPrevButtonClicked(e) {
        const target = this._items[this._currentItemIndex - 1];

        if (target !== undefined) {
            this._nextButton.removeAttribute('disabled');

            this._itemWrapper.scrollTo({
                top: target.offsetTop,
                behavior: this.options.scrollBehavior
            });

            this._currentItemIndex--;

            this._toggleActiveItem(target);

            if (this._currentItemIndex === 0) {
                this._prevButton.setAttribute('disabled', '');
            }

            document.$emitter.publish('timeline-index-changed', {
                itemIndex: this._currentItemIndex
            });

            if (window.debug === true) {
                console.log(`%cScroll to previous timeline element (${target.offsetTop})`, 'color: #666')
            }
        }
    }

    onNextButtonClicked(e) {
        const target = this._items[this._currentItemIndex + 1];

        if (target !== undefined) {
            this._prevButton.removeAttribute('disabled');

            this._itemWrapper.scrollTo({
                top: target.offsetTop,
                behavior: this.options.scrollBehavior
            });

            this._currentItemIndex++;

            this._toggleActiveItem(target);

            if (this._currentItemIndex === this._items.length - 1) {
                this._nextButton.setAttribute('disabled', '');
            }

            document.$emitter.publish('timeline-index-changed', {
                itemIndex: this._currentItemIndex
            });

            if (window.debug === true) {
                console.log(`%cScroll to next timeline element (${target.offsetTop})`, 'color: #666')
            }
        }
    }

    _toggleActiveItem(target) {
        this._items.forEach((item) => item.classList.remove('active'));
        target.classList.add('active');
    }

    _loadItems() {
        let timelineItems = DomAccess.querySelectorAll(this.el, this.options.itemSelector, false);

        if (timelineItems) {
            Array.from(timelineItems).forEach((item, key) => {
                const {headline, content} = this._getItemChildren(item)

                if (!headline || !content) {
                    item.remove()
                }

                this._totalHeight += item.getBoundingClientRect().height;
                this._positions[key] = (this._positions[key - 1] ?? 0) + item.getBoundingClientRect().height;

                item.setAttribute('data-timeline-item-id', key )

                if (key === 0) {
                    item.classList.add('active', 'first')
                } else if (key === timelineItems.length - 1) {
                    item.classList.add('last')
                }

                const divider = document.createElement('div');
                divider.classList.add(this.options.itemDividerSelector);

                item.insertBefore(divider, content)

                this._items[key] = item;
            })
        }
    }

    /**
     * @param item {HTMLElement}
     * @returns {{headline: HTMLElement, content: HTMLElement}}
     * @private
     */
    _getItemChildren(item) {
        return {
            headline: DomAccess.querySelector(item, this.options.itemHeadlineSelector, false),
            content: DomAccess.querySelector(item, this.options.itemContentSelector, false)
        }
    }
}
