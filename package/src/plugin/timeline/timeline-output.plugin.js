import {Plugin} from '../../plugin-system';
import {DomAccess, StringHelper} from "../../helper";
import './timeline.output.scss'

export default class TimelineOutputPlugin extends Plugin {

    static options = Object.preventExtensions({
        strict: true, // hides last element when current element not exists
    })

    static _currentItemIndex = 0;

    init() {
        this._currentItemIndex = 0;

        if (this.el.dataset.timelineId !== undefined) {
            document.$emitter.subscribe('timeline-registered', (event) => {

                this._toggleElements();

                if (event.detail.id === this.el.dataset.timelineId) {
                    document.$emitter.subscribe('timeline-index-changed', (event) => {
                        this._currentItemIndex = event.detail.itemIndex;
                        this._toggleElements();
                    })
                }
            })
        } else {
            throw new Error('You need to provide an id for the timeline output')
        }
    }

    _prepareElements() {
        const allItems = DomAccess.querySelectorAll(this.el, '[data-timeline-output-id]', false);

        if (allItems) {
            Array.from(allItems).forEach((item) => {
                if (item.dataset.timelineOutputId.length > 0) {
                    let ids = StringHelper.isJson(item.dataset.timelineOutputId)
                        ? JSON.parse(item.dataset.timelineOutputId)
                        : item.dataset.timelineOutputId;

                    if (Array.isArray(ids)) {
                        ids.forEach((id) => {
                            const tmp = item.cloneNode(true);
                            tmp.setAttribute('data-timeline-output-id', id);

                            item.parentNode.insertBefore(tmp, item);
                        })

                        item.parentNode.removeChild(item)
                    }
                }
            })
        }
    }

    _toggleElements() {
        this._prepareElements();

        const currentSelector = `[data-timeline-output-id="${this._currentItemIndex}"]`;

        const allItems = DomAccess.querySelectorAll(this.el, '[data-timeline-output-id]', false);
        const target = DomAccess.querySelector(this.el, currentSelector, false);

        if (target && allItems && !this.options.strict) {
            Array.from(allItems).forEach((item) => {
                if (item === target && !item.classList.contains('active')) {
                    item.classList.add('active');
                } else if (item !== target && item.classList.contains('active')) {
                    item.classList.remove('active');
                }
            })
        } else if (this.options.strict) {
            if (allItems) {
                Array.from(allItems).forEach((item) => {
                    if (item.classList.contains('active')) {
                        item.classList.remove('active');
                    }
                })
            }

            if (target) {
                if (!target.classList.contains('active')) {
                    target.classList.add('active');
                }
            }
        }
    }
}
