import {Plugin} from '../../plugin-system';
import './info-circle.scss'
import {BORDER_STYLE, TRANSITION_TIMING, CURSOR} from "../../constant";

export default class InfoCirclePlugin extends Plugin {

    static options = {
        elementSelector: '[data-info-circle-element]',
        elementIconSelector: '[data-info-circle-element-icon]',
        elementContentSelector: '[data-info-circle-element-content]',

        outerCircleBorderThickness: '10px',
        outerCircleBorderColor: '#F0F0F0',
        outerCircleBorderStyle: BORDER_STYLE.SOLID,

        innerCircleBorderThickness: '1px',
        innerCircleBorderColor: '#ccc',
        innerCircleBorderStyle: BORDER_STYLE.SOLID,
        innerCircleOffset: '110px',
        innerCircleColor: '#fff',
        innerCircleBackground: '#4D4D4D',
        innerCircleContentPadding: '15px',

        elementBorderThickness: '2px',
        elementBorderColor: '#ccc',
        elementBorderStyle: BORDER_STYLE.SOLID,
        elementSize: '70px',
        elementColor: '#fff',
        elementBackground: '#007BC4',
        elementCursor: CURSOR.POINTER,

        elementHoverColor: '#fff',
        elementHoverBackground: '#4D4D4D',
        elementHoverScale: 1.1,
        elementHoverTransitionDuration: '.2s',
        elementHoverTransitionTiming: TRANSITION_TIMING.EASE,

        elementActiveColor: '#fff',
        elementActiveBackground: '#4D4D4D',
    }

    init() {
        this.el.style.display = 'none';
        this._infoCircleElements = this._loadElements();

        this._render();
        this.$emitter.publish('afterInitInfoCircle');
    }

    _render() {
        this._outerCircle = this._getOuterCircle();
        this._innerCircle = this._getInnerCircle();

        this._iconElements = this._getIconElements();

        this.el.appendChild(this._outerCircle);
        this.el.appendChild(this._innerCircle);

        this._iconElements.forEach((iconElement) => this.el.appendChild(iconElement));

        this.el.style.display = 'block';
    }

    _getIconElements() {
        return this._infoCircleElements.map((infoCircleElement) => {
            const iconElement = infoCircleElement.icon;
            const iconElementWrapper = document.createElement('div');
            const iconElementRotator = document.createElement('div');

            iconElement.classList.add('info-circle--element-icon');
            iconElementWrapper.classList.add('info-circle--element');
            iconElementRotator.classList.add('info-circle--element-rotator');

            const rotation = `${((360 / this._infoCircleElements.length) * infoCircleElement.index) + 180}deg`;

            iconElementRotator.style.transform = `rotate(${rotation})`;
            iconElement.style.transform = `rotate(-${rotation})`;

            iconElementRotator.style.setProperty('--ic-element-rotation', rotation);

            iconElementWrapper.style.setProperty('--ic-element-border-thickness', this.options.elementBorderThickness);
            iconElementWrapper.style.setProperty('--ic-element-border-color', this.options.elementBorderColor);
            iconElementWrapper.style.setProperty('--ic-element-border-style', this.options.elementBorderStyle);
            iconElementWrapper.style.setProperty('--ic-element-size', this.options.elementSize);

            iconElementWrapper.style.setProperty('--ic-element-color', this.options.elementColor);
            iconElementWrapper.style.setProperty('--ic-element-background', this.options.elementBackground);

            iconElementWrapper.style.setProperty('--ic-active-element-color', this.options.elementActiveColor);
            iconElementWrapper.style.setProperty('--ic-active-element-background', this.options.elementActiveBackground);

            iconElementWrapper.style.setProperty('--ic-hover-element-color', this.options.elementHoverColor);
            iconElementWrapper.style.setProperty('--ic-hover-element-background', this.options.elementHoverBackground);
            iconElementWrapper.style.setProperty('--ic-hover-element-scale', this.options.elementHoverScale);

            iconElementWrapper.style.setProperty('--ic-hover-element-transition-duration', this.options.elementHoverTransitionDuration);
            iconElementWrapper.style.setProperty('--ic-hover-element-transition-timing', this.options.elementHoverTransitionTiming);

            iconElementWrapper.addEventListener('click', this._onClickElement.bind(this))

            if (infoCircleElement.index === 0) {
                iconElementWrapper.classList.add('info-circle--element__active');
            }

            iconElementWrapper.setAttribute('data-info-circle-element-id', infoCircleElement.index)

            iconElementWrapper.appendChild(iconElement);
            iconElementRotator.appendChild(iconElementWrapper);

            return iconElementRotator;
        })
    }

    _setContent(target) {
        const iconElementWrapperId = parseInt(target.dataset.infoCircleElementId);

        const targetElementData = this._infoCircleElements.find((e) => e.index === iconElementWrapperId)

        Array.from(this._innerCircle.childNodes).forEach((childNode) => {
            this._innerCircle.removeChild(childNode);
        })

        if (targetElementData) {
            if (targetElementData.content instanceof HTMLElement) {
                targetElementData.content.classList.add('info-circle--element-content')

                let shape = targetElementData.content.querySelector('.info-circle--element-content-shape');

                if (shape === null) {
                    shape = document.createElement('i');
                    shape.classList.add('info-circle--element-content-shape');

                    targetElementData.content.innerHTML = shape.outerHTML + targetElementData.content.innerHTML;
                }

                targetElementData.content.style.setProperty('--ic-inner-circle-content-padding', this.options.innerCircleContentPadding);
            }

            this._innerCircle.appendChild(targetElementData.content)
        }
    }

    _onClickElement(e) {
        const targetIconElementWrapper = e.target;
        const targetIconElementRotator = e.target.parentNode;

        const targetIconElementRotatorAngle = this._getRotationAngle(targetIconElementRotator, '--ic-element-rotation');

        this._setContent(targetIconElementWrapper);

        this.$emitter.publish('onClickElement');

        this._iconElements.forEach((iconElementRotator) => {
            const iconElementWrapper = iconElementRotator.querySelector('.info-circle--element')
            const iconElement = iconElementRotator.querySelector('.info-circle--element-icon')

            const iconElementRotatorAngle = this._getRotationAngle(iconElementRotator, '--ic-element-rotation');
            const rotatorRotation = iconElementRotatorAngle - targetIconElementRotatorAngle + 180;

            iconElementWrapper.classList.remove('info-circle--element__active');

            iconElementRotator.style.transform = `rotate(${rotatorRotation}deg)`;
            iconElement.style.transform = `rotate(${rotatorRotation * -1}deg)`;

            iconElementRotator.style.setProperty('--ic-element-rotation', rotatorRotation)
        });

        targetIconElementWrapper.classList.add('info-circle--element__active');
    }

    /**
     * @param {HTMLElement} element
     * @param {string} property
     * @returns {number}
     * @private
     */
    _getRotationAngle(element, property) {
        const propertyValue = element.style.getPropertyValue(property);
        return parseInt(propertyValue.replace('deg', ''));
    }

    _getInnerCircle() {
        const element = document.createElement('div');

        element.classList.add('info-circle--inner')

        element.style.setProperty('--ic-inner-circle-offset', this.options.innerCircleOffset);
        element.style.setProperty('--ic-inner-circle-border-thickness', this.options.innerCircleBorderThickness);
        element.style.setProperty('--ic-inner-circle-border-color', this.options.innerCircleBorderColor);
        element.style.setProperty('--ic-inner-circle-border-style', this.options.innerCircleBorderStyle);
        element.style.setProperty('--ic-inner-circle-color', this.options.innerCircleColor);
        element.style.setProperty('--ic-inner-circle-background', this.options.innerCircleBackground);

        if (this._infoCircleElements.length >= 1) {
            const content = this._infoCircleElements[0].content;

            if (content instanceof HTMLElement) {
                content.classList.add('info-circle--element-content')

                let shape = content.querySelector('.info-circle--element-content-shape');

                if (shape === null) {
                    shape = document.createElement('i');
                    shape.classList.add('info-circle--element-content-shape');

                    content.innerHTML = shape.outerHTML + content.innerHTML;
                }

                content.style.setProperty('--ic-inner-circle-content-padding', this.options.innerCircleContentPadding);
            }

            element.appendChild(content)
        }

        return element;
    }

    _getOuterCircle() {
        const element = document.createElement('div');

        element.classList.add('info-circle--outer');

        element.style.setProperty('--ic-outer-circle-border-thickness', this.options.outerCircleBorderThickness);
        element.style.setProperty('--ic-outer-circle-border-color', this.options.outerCircleBorderColor);
        element.style.setProperty('--ic-outer-circle-border-style', this.options.outerCircleBorderStyle);

        return element;
    }

    _loadElements() {
        const infoCircleElements = this.el.querySelectorAll(this.options.elementSelector);

        const elements = Array.from(infoCircleElements).map((infoCircleElement, key) => {
            const infoCircleElementIcon = infoCircleElement.querySelector(this.options.elementIconSelector)
            const infoCircleElementContent = infoCircleElement.querySelector(this.options.elementContentSelector)

            if (!infoCircleElementIcon) {
                if (window.debug === true) {
                    console.log(`%cMissing Icon Element in Info Slider`, 'color: #f00')
                }
                return null;
            }

            if (!infoCircleElementContent) {
                if (window.debug === true) {
                    console.log(`%cMissing Content Element in Info Slider`, 'color: #f00')
                }
                return null;
            }

            this.el.removeChild(infoCircleElement)

            infoCircleElementIcon.removeAttribute(this._getCleanedSelector(this.options.elementIconSelector))
            infoCircleElementContent.removeAttribute(this._getCleanedSelector(this.options.elementContentSelector))

            return {
                index: key,
                icon: infoCircleElementIcon,
                content: infoCircleElementContent
            }
        }).filter((element) => element);

        if (elements.length === 0 && window.debug === true) {
            console.log(`%cInfo Slider has no elements`, 'color: #f00')
        }

        return elements
    }

    _getCleanedSelector(selector) {
        return selector.replace(/[\[\]']+/g,'');
    }
}
