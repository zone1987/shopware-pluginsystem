import deepmerge from 'deepmerge';
import CountUpPlugin from "../count-up/count-up.plugin";
import {CountUp} from 'countup.js';
import './progressbar.scss';

export default class ProgressbarPlugin extends CountUpPlugin {

    static options = deepmerge(CountUpPlugin.options, {
        max: 100,
        value: 100,
        suffix: ' %'
    });

    static counter;
    static progressbar;

    init() {
        let max = this.options.max;
        let value = this.options.value;

        if (!isNaN(max) && !isNaN(parseFloat(max))) {
            const label = this.el.textContent;

            // Sets countup value
            this.counter = this._getCounter(value);
            this.progressbar = this._getProgressbar(value, max);

            const textWrapper = this._getTextWrapper()

            this.el.textContent = '';

            textWrapper.appendChild(this._getLabel(label))
            textWrapper.appendChild(this.counter)

            this.el.appendChild(textWrapper);
            this.el.appendChild(this.progressbar)

            this.options.formattingFn = this._onFormatting.bind(this);

            new CountUp(this.counter, this.counter.textContent, this.options);
        }
    }

    _onFormatting(n) {
        this.progressbar.value = n;

        return n + this.options.suffix;
    }

    /**
     *
     * @param {string|number} value
     * @param {string|number} max
     * @returns {HTMLElement}
     * @private
     */
    _getProgressbar(value, max) {
        let element = document.createElement('progress');
        element.setAttribute('max', max.toString());
        element.setAttribute('value', value.toString());

        return element;
    }

    /**
     *
     * @param {string|number} value
     * @returns {HTMLElement}
     * @private
     */
    _getCounter(value = 100) {
        let element = document.createElement('p');
        element.textContent = value.toString()
        element.classList.add('progressbar-counter');

        return element;
    }

    /**
     *
     * @param {string} label
     * @returns {HTMLElement}
     * @private
     */
    _getLabel(label = '') {
        let element = document.createElement('p');
        element.textContent = label
        element.classList.add('progressbar-label');

        return element;
    }

    /**
     *
     * @returns {HTMLElement}
     * @private
     */
    _getTextWrapper() {
        let element = document.createElement('div');
        element.classList.add('progressbar-text-wrapper');

        return element;
    }
}
