import {Plugin} from '../../plugin-system';
import {CountUp} from 'countup.js';

export default class CountUpPlugin extends Plugin {

    static options = {
        startVal: 0, // number to start at (0)
        decimalPlaces: 0, // number of decimal places (0)
        duration: 4, // animation duration in seconds (2)
        useGrouping: true, // example: 1,000 vs 1000 (true)
        useIndianSeparators: false, // example: 1,00,000 vs 100,000 (false)
        useEasing: true, // ease animation (true)
        smartEasingThreshold: 999, // smooth easing for large numbers above this if useEasing (999)
        smartEasingAmount: 333, // amount to be eased for numbers above threshold (333)
        separator: '.', // grouping separator (',')
        decimal: ',', // decimal ('.')
        prefix: '', // text prepended to result
        suffix: '', // text appended to result
        numerals: [], // numeral glyph substitution
        enableScrollSpy: true, // start animation when target is in view
        scrollSpyDelay: 0, // delay (ms) after target comes into view
        scrollSpyOnce: false
    }

    init() {
        new CountUp(this.el, this.el.textContent, this.options);
    }
}