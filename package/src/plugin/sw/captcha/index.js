import BasicCaptchaPlugin from "./basic-captcha.plugin";
import GoogleReCaptchaV2Plugin from "./google-re-captcha/google-re-captcha-v2.plugin";
import GoogleReCaptchaV3Plugin from "./google-re-captcha/google-re-captcha-v3.plugin";

export {
    BasicCaptchaPlugin,
    GoogleReCaptchaV2Plugin,
    GoogleReCaptchaV3Plugin
}