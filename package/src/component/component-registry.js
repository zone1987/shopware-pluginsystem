import {ImageHotspot} from "./image-hotspot";

export default class ComponentRegistry {

    static _components = [
        ImageHotspot
    ]

    /**
     * @param prefix {string}
     */
    static init(prefix = '') {
        this._components.forEach((component) => {
            window.customElements.define(`${prefix}${component.componentName}`, component);

            if (window.debug === true) {
                console.log(`%cComponent: ${prefix}${component.componentName} loaded`, 'color: #666')
            }
        })
    }
}
