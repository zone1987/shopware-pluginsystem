import './image-hotspot.scss'

export default class ImageHotspot extends HTMLElement {

    /**
     * @type {string}
     */
    static componentName = 'image-hotspot';

    _positionClasses = Object.freeze({
        static: 'position-static',
        absolute: 'position-absolute',
        fixed: 'position-fixed',
        relative: 'position-relative',
        sticky: 'position-sticky'
    })

    /**
     * @type {0|string}
     */
    top = 0;

    /**
     * @type {0|string}
     */
    left = 0;

    /**
     * @type {string}
     */
    placement = 'auto';

    /**
     * @type {string}
     */
    trigger = 'hover';

    /**
     * @type {string}
     */
    title = '';

    /**
     * @type {string}
     */
    content = '';

    /**
     * @type {HTMLElement}
     * @private
     */
    _title = undefined;

    /**
     * @type {HTMLElement}
     * @private
     */
    _content = undefined;

    constructor() {
        super();

        this.classList.add(ImageHotspot.componentName);

        this._prepareParentElement();
        this._prepareElement();
    }

    _prepareElement() {
        this.title = this.querySelector('[slot=title]');
        this.content = this.querySelector('[slot=content]');

        this.title.removeAttribute('slot');
        this.content.removeAttribute('slot');

        this.setAttribute('data-bs-toggle', 'popover');
        this.setAttribute('data-bs-title', 'title');
        this.setAttribute('data-bs-content', 'content');

        this.innerHTML = '';

        const button = document.createElement('div');
        button.classList.add(`${ImageHotspot.componentName}-button`);
        this.appendChild(button);

        if (this.hasAttribute('placement')) {
            this.placement = this.getAttribute('placement');
        }

        if (this.hasAttribute('trigger')) {
            this.trigger = this.getAttribute('trigger');
        }

        const config = {
            title: this.title,
            content: this.content,
            placement: this.placement,
            html: true
        }

        if (this.trigger === 'hover') {
            config['trigger'] = this.trigger;
        }

        new bootstrap.Popover(this, config)
    }

    _prepareParentElement() {
        Object.values(this._positionClasses).forEach((positionClass) => {
            if (this.parentElement.classList.contains(positionClass)) {
                this.parentElement.classList.remove(positionClass);
            }
        });

        this.parentElement.classList.add(this._positionClasses.relative);
    }

    /**
     * @returns {string[]}
     */
    static get observedAttributes() {
        return ['top', 'left', 'placement', 'trigger', 'title', 'content'];
    }

    /**
     * @param name {string}
     * @param oldValue
     * @param newValue
     */
    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue !== newValue) {
            if (['top', 'left'].includes(name)) {
                this[name] = newValue;
                this.setAttribute(name, newValue);
                this.style.setProperty(`--image-hotspot-position-${name}`, newValue);
            }
        }
    }
}
