export const SCROLL_BEHAVIOR = Object.freeze({
    AUTO: 'auto',
    SMOOTH: 'smooth',
    INITIAL: 'initial',
    INHERIT: 'inherit'
});
