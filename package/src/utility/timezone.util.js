const TIMEZONE_COOKIE = 'timezone';

import {CookieStorage} from "../helper";

/**
 * @package storefront
 */
export default class TimezoneUtil {

    /**
     * Constructor
     */
    constructor() {
        if (!CookieStorage.isSupported()) {
            return;
        }

        CookieStorage.setItem(
            TIMEZONE_COOKIE,
            Intl.DateTimeFormat().resolvedOptions().timeZone,
            30
        );
    }

}
