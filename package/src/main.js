// import helpers
import {PluginManager} from "./plugin-system";
import {NativeEventEmitter, ViewportDetection} from "./helper";
import ComponentRegistry from "./component/component-registry";

// import utils
import {TimezoneUtil} from "./utility";

export default class PluginSystem {

    static init(debug = false) {
        window.debug = debug;
        window.eventEmitter = new NativeEventEmitter();
        window.breakpoints = Object.preventExtensions({
            xs: 0,
            sm: 576,
            md: 768,
            lg: 992,
            xl: 1200,
            xxl: 1400
        });

        if (window.debug === true) {
            console.log('%cBreakpoints (Accessor: window.breakpoints)', 'color: #FFCA28')
            console.table(window.breakpoints)
            console.log("\n")
        }

        ComponentRegistry.init('at-')

        // initialisation
        new ViewportDetection();

        // run plugins
        document.addEventListener('DOMContentLoaded', () => PluginManager.initializePlugins(true), false);

        // run utils
        new TimezoneUtil();
    }
}
